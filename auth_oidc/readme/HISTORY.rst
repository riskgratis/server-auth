12.0.1.2.0 2022-11-22
~~~~~~~~~~~~~~~~~~~~~

* Fix handling OpenID Connect responses without custom mapping  by using the ``sub``  claim as user id
* Fix handling OpenID Connect ID Tokens without Key ID (``kid``)

12.0.1.1.0 2022-11-22
~~~~~~~~~~~~~~~~~~~~~

* Enable allowing assigning groups from token claims

12.0.1.0.1 2022-02-28
~~~~~~~~~~~~~~~~~~~~~

* Updated readme and pot

12.0.1.0.0 2022-02-12
~~~~~~~~~~~~~~~~~~~~~

* Backport to Odoo 12

13.0.1.0.0 2020-04-10
~~~~~~~~~~~~~~~~~~~~~

* Odoo 13 migration, add authorization code flow.

10.0.1.0.0 2018-10-05
~~~~~~~~~~~~~~~~~~~~~

* Initial implementation
